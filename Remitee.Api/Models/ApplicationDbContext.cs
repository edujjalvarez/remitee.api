﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Remitee.Api.Models.Currencies;
using Remitee.Api.Models.Quotes;

namespace Remitee.Api.Models
{
    public class ApplicationDbContext : DbContext
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public ApplicationDbContext(
            DbContextOptions options,
            IHttpContextAccessor httpContextAccessor) : base(options)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurations();
            modelBuilder.Seed();
        }

        public DbSet<Currency> Currencies { get; set; }

        public DbSet<Quote> Quotes { get; set; }

        public override int SaveChanges()
        {
            var currentUser = string.IsNullOrEmpty(_httpContextAccessor?.HttpContext?.User?.Identity?.Name) ? "Anonymous" : _httpContextAccessor?.HttpContext?.User?.Identity?.Name;

            foreach (var entry in ChangeTracker.Entries<BaseModel>())
            {
                if (entry.State == EntityState.Added || entry.State == EntityState.Modified)
                {
                    if (entry.State == EntityState.Added)
                    {
                        if (string.IsNullOrEmpty(entry.Entity.Id))
                        {
                            entry.Entity.Id = Guid.NewGuid().ToString();
                        }
                        entry.Entity.RegisterDate = entry.Entity.RegisterDate ?? DateTime.UtcNow;
                        entry.Entity.RegisterBy = currentUser;
                    }
                    else
                    {
                        entry.Property(p => p.RegisterDate).IsModified = false;
                        entry.Property(p => p.RegisterBy).IsModified = false;
                    }

                    entry.Entity.UpdatedDate = DateTime.UtcNow;
                    entry.Entity.UpdatedBy = currentUser;
                }

                if (entry.State == EntityState.Deleted)
                {
                    SoftDelete(entry, this, currentUser);
                }
            }
            return base.SaveChanges();
        }

        public void SoftDelete(EntityEntry<BaseModel> entry, ApplicationDbContext context, string deletedBy)
        {
            var properties = entry.OriginalValues.Properties;
            foreach (var property in properties)
            {
                entry.Property(property.Name).CurrentValue = entry.OriginalValues[property.Name];
            }
            entry.Property("IsDeleted").CurrentValue = true;
            entry.Property("DeletedDate").CurrentValue = DateTime.UtcNow;
            entry.Property("DeletedBy").CurrentValue = deletedBy;
            entry.State = EntityState.Modified;
        }
    }
}
