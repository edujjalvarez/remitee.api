﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Remitee.Api.Models
{
    public class BaseDto
    {
        [StringLength(64)]
        public string Id { get; set; } = Guid.NewGuid().ToString();
    }
}
