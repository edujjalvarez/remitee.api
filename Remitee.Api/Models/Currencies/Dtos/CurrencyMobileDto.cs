﻿using System.ComponentModel.DataAnnotations;

namespace Remitee.Api.Models.Currencies.Dtos
{
    public class CurrencyMobileDto : BaseDto
    {
        [Required]
        [StringLength(64)]
        public string Name { get; set; }
    }
}
