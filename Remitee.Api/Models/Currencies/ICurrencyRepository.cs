﻿namespace Remitee.Api.Models.Currencies
{
    public interface ICurrencyRepository : IBaseRepository<Currency>
    {
    }
}
