﻿namespace Remitee.Api.Models.Quotes
{
    public class QuoteRepository : BaseRepository<Quote>, IQuoteRepository
    {
        public QuoteRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}
