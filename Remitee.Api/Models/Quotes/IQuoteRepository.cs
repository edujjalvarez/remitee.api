﻿namespace Remitee.Api.Models.Quotes
{
    public interface IQuoteRepository : IBaseRepository<Quote>
    {
    }
}
