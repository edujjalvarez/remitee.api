﻿using Microsoft.EntityFrameworkCore;
using Remitee.Api.Models.Quotes;

namespace Remitee.Api.Migrations.Seed
{
    public static class QuoteSeed
    {
        public static void SeedQuotes(this ModelBuilder model)
        {
            model.Entity<Quote>().HasData(
                new Quote { Id = "USDAED", Source = "USD", Value = 3.67303 },
                new Quote { Id = "USDAFN", Source = "USD", Value = 88.373834 },
                new Quote { Id = "USDALL", Source = "USD", Value = 112.575769 },
                new Quote { Id = "USDAMD", Source = "USD", Value = 512.67351 },
                new Quote { Id = "USDANG", Source = "USD", Value = 1.800281 },
                new Quote { Id = "USDAOA", Source = "USD", Value = 469.912007 },
                new Quote { Id = "USDARS", Source = "USD", Value = 109.192029 },
                new Quote { Id = "USDAUD", Source = "USD", Value = 1.390105 },
                new Quote { Id = "USDAWG", Source = "USD", Value = 1.80025 },
                new Quote { Id = "USDAZN", Source = "USD", Value = 1.703123 },
                new Quote { Id = "USDBAM", Source = "USD", Value = 1.785478 },
                new Quote { Id = "USDBBD", Source = "USD", Value = 2.001497 },
                new Quote { Id = "USDBDT", Source = "USD", Value = 85.98543 },
                new Quote { Id = "USDBGN", Source = "USD", Value = 1.77943 },
                new Quote { Id = "USDBHD", Source = "USD", Value = 0.376993 },
                new Quote { Id = "USDBIF", Source = "USD", Value = 2052.416337 },
                new Quote { Id = "USDBMD", Source = "USD", Value = 1 },
                new Quote { Id = "USDBND", Source = "USD", Value = 1.364024 },
                new Quote { Id = "USDBOB", Source = "USD", Value = 6.862912 },
                new Quote { Id = "USDBRL", Source = "USD", Value = 5.122876 },
                new Quote { Id = "USDBSD", Source = "USD", Value = 0.998968 },
                new Quote { Id = "USDBTC", Source = "USD", Value = 2.6077092e-5 },
                new Quote { Id = "USDBTN", Source = "USD", Value = 76.446487 },
                new Quote { Id = "USDBWP", Source = "USD", Value = 11.622271 },
                new Quote { Id = "USDBYN", Source = "USD", Value = 3.289925 },
                new Quote { Id = "USDBYR", Source = "USD", Value = 19600 },
                new Quote { Id = "USDBZD", Source = "USD", Value = 2.002866 },
                new Quote { Id = "USDCAD", Source = "USD", Value = 1.284405 },
                new Quote { Id = "USDCDF", Source = "USD", Value = 2017.999741 },
                new Quote { Id = "USDCHF", Source = "USD", Value = 0.93791 },
                new Quote { Id = "USDCLF", Source = "USD", Value = 0.029482 },
                new Quote { Id = "USDCLP", Source = "USD", Value = 813.501861 },
                new Quote { Id = "USDCNY", Source = "USD", Value = 6.379021 },
                new Quote { Id = "USDCOP", Source = "USD", Value = 3797.07 },
                new Quote { Id = "USDCRC", Source = "USD", Value = 646.991702 },
                new Quote { Id = "USDCUC", Source = "USD", Value = 1 },
                new Quote { Id = "USDCUP", Source = "USD", Value = 26.5 },
                new Quote { Id = "USDCVE", Source = "USD", Value = 100.662318 },
                new Quote { Id = "USDCZK", Source = "USD", Value = 22.583027 },
                new Quote { Id = "USDDJF", Source = "USD", Value = 178.158349 },
                new Quote { Id = "USDDKK", Source = "USD", Value = 6.75713 },
                new Quote { Id = "USDDOP", Source = "USD", Value = 54.894658 },
                new Quote { Id = "USDDZD", Source = "USD", Value = 142.770974 },
                new Quote { Id = "USDEGP", Source = "USD", Value = 15.706898 },
                new Quote { Id = "USDERN", Source = "USD", Value = 15.000005 },
                new Quote { Id = "USDETB", Source = "USD", Value = 51.35969 },
                new Quote { Id = "USDEUR", Source = "USD", Value = 0.90811 },
                new Quote { Id = "USDFJD", Source = "USD", Value = 2.12455 },
                new Quote { Id = "USDFKP", Source = "USD", Value = 0.766959 },
                new Quote { Id = "USDGBP", Source = "USD", Value = 0.767225 },
                new Quote { Id = "USDGEL", Source = "USD", Value = 3.219841 },
                new Quote { Id = "USDGGP", Source = "USD", Value = 0.766959 },
                new Quote { Id = "USDGHS", Source = "USD", Value = 7.142629 },
                new Quote { Id = "USDGIP", Source = "USD", Value = 0.766959 },
                new Quote { Id = "USDGMD", Source = "USD", Value = 53.350212 },
                new Quote { Id = "USDGNF", Source = "USD", Value = 8947.414065 },
                new Quote { Id = "USDGTQ", Source = "USD", Value = 7.697589 },
                new Quote { Id = "USDGYD", Source = "USD", Value = 209.120911 },
                new Quote { Id = "USDHKD", Source = "USD", Value = 7.82379 },
                new Quote { Id = "USDHNL", Source = "USD", Value = 24.637827 },
                new Quote { Id = "USDHRK", Source = "USD", Value = 6.881798 },
                new Quote { Id = "USDHTG", Source = "USD", Value = 103.951326 },
                new Quote { Id = "USDHUF", Source = "USD", Value = 339.191022 },
                new Quote { Id = "USDIDR", Source = "USD", Value = 14322 },
                new Quote { Id = "USDILS", Source = "USD", Value = 3.28269 },
                new Quote { Id = "USDIMP", Source = "USD", Value = 0.766959 },
                new Quote { Id = "USDINR", Source = "USD", Value = 76.472497 },
                new Quote { Id = "USDIQD", Source = "USD", Value = 1458.081714 },
                new Quote { Id = "USDIRR", Source = "USD", Value = 42300.000334 },
                new Quote { Id = "USDISK", Source = "USD", Value = 131.789814 },
                new Quote { Id = "USDJEP", Source = "USD", Value = 0.766959 },
                new Quote { Id = "USDJMD", Source = "USD", Value = 153.038906 },
                new Quote { Id = "USDJOD", Source = "USD", Value = 0.709004 },
                new Quote { Id = "USDJPY", Source = "USD", Value = 117.997999 },
                new Quote { Id = "USDKES", Source = "USD", Value = 114.249778 },
                new Quote { Id = "USDKGS", Source = "USD", Value = 104.963201 },
                new Quote { Id = "USDKHR", Source = "USD", Value = 4068.263872 },
                new Quote { Id = "USDKMF", Source = "USD", Value = 448.350109 },
                new Quote { Id = "USDKPW", Source = "USD", Value = 900.00035 },
                new Quote { Id = "USDKRW", Source = "USD", Value = 1244.734976 },
                new Quote { Id = "USDKWD", Source = "USD", Value = 0.30406 },
                new Quote { Id = "USDKYD", Source = "USD", Value = 0.832398 },
                new Quote { Id = "USDKZT", Source = "USD", Value = 518.203728 },
                new Quote { Id = "USDLAK", Source = "USD", Value = 11503.181457 },
                new Quote { Id = "USDLBP", Source = "USD", Value = 1510.126392 },
                new Quote { Id = "USDLKR", Source = "USD", Value = 254.739408 },
                new Quote { Id = "USDLRD", Source = "USD", Value = 153.849901 },
                new Quote { Id = "USDLSL", Source = "USD", Value = 15.069759 },
                new Quote { Id = "USDLTL", Source = "USD", Value = 2.95274 },
                new Quote { Id = "USDLVL", Source = "USD", Value = 0.60489 },
                new Quote { Id = "USDLYD", Source = "USD", Value = 4.630693 },
                new Quote { Id = "USDMAD", Source = "USD", Value = 9.690709 },
                new Quote { Id = "USDMDL", Source = "USD", Value = 18.418941 },
                new Quote { Id = "USDMGA", Source = "USD", Value = 4039.399693 },
                new Quote { Id = "USDMKD", Source = "USD", Value = 55.972318 },
                new Quote { Id = "USDMMK", Source = "USD", Value = 1776.251095 },
                new Quote { Id = "USDMNT", Source = "USD", Value = 2878.772791 },
                new Quote { Id = "USDMOP", Source = "USD", Value = 8.056472 },
                new Quote { Id = "USDMRO", Source = "USD", Value = 356.999828 },
                new Quote { Id = "USDMUR", Source = "USD", Value = 44.95029 },
                new Quote { Id = "USDMVR", Source = "USD", Value = 15.450163 },
                new Quote { Id = "USDMWK", Source = "USD", Value = 808.769992 },
                new Quote { Id = "USDMXN", Source = "USD", Value = 20.85744 },
                new Quote { Id = "USDMYR", Source = "USD", Value = 4.208049 },
                new Quote { Id = "USDMZN", Source = "USD", Value = 63.830328 },
                new Quote { Id = "USDNAD", Source = "USD", Value = 15.070058 },
                new Quote { Id = "USDNGN", Source = "USD", Value = 415.320236 },
                new Quote { Id = "USDNIO", Source = "USD", Value = 35.855843 },
                new Quote { Id = "USDNOK", Source = "USD", Value = 9.02048 },
                new Quote { Id = "USDNPR", Source = "USD", Value = 122.306407 },
                new Quote { Id = "USDNZD", Source = "USD", Value = 1.480875 },
                new Quote { Id = "USDOMR", Source = "USD", Value = 0.38449 },
                new Quote { Id = "USDPAB", Source = "USD", Value = 0.998895 },
                new Quote { Id = "USDPEN", Source = "USD", Value = 3.699072 },
                new Quote { Id = "USDPGK", Source = "USD", Value = 3.5198 },
                new Quote { Id = "USDPHP", Source = "USD", Value = 52.395497 },
                new Quote { Id = "USDPKR", Source = "USD", Value = 179.178412 },
                new Quote { Id = "USDPLN", Source = "USD", Value = 4.29425 },
                new Quote { Id = "USDPYG", Source = "USD", Value = 6959.093689 },
                new Quote { Id = "USDQAR", Source = "USD", Value = 3.640975 },
                new Quote { Id = "USDRON", Source = "USD", Value = 4.494897 },
                new Quote { Id = "USDRSD", Source = "USD", Value = 106.880204 },
                new Quote { Id = "USDRUB", Source = "USD", Value = 111.085101 },
                new Quote { Id = "USDRWF", Source = "USD", Value = 1019.471806 },
                new Quote { Id = "USDSAR", Source = "USD", Value = 3.75159 },
                new Quote { Id = "USDSBD", Source = "USD", Value = 8.048354 },
                new Quote { Id = "USDSCR", Source = "USD", Value = 14.414873 },
                new Quote { Id = "USDSDG", Source = "USD", Value = 447.507578 },
                new Quote { Id = "USDSEK", Source = "USD", Value = 9.57172 },
                new Quote { Id = "USDSGD", Source = "USD", Value = 1.365675 },
                new Quote { Id = "USDSHP", Source = "USD", Value = 1.377401 },
                new Quote { Id = "USDSLL", Source = "USD", Value = 11719.999966 },
                new Quote { Id = "USDSOS", Source = "USD", Value = 588.000384 },
                new Quote { Id = "USDSRD", Source = "USD", Value = 20.603497 },
                new Quote { Id = "USDSTD", Source = "USD", Value = 20697.981008 },
                new Quote { Id = "USDSVC", Source = "USD", Value = 8.740466 },
                new Quote { Id = "USDSYP", Source = "USD", Value = 2512.000338 },
                new Quote { Id = "USDSZL", Source = "USD", Value = 15.075403 },
                new Quote { Id = "USDTHB", Source = "USD", Value = 33.52499 },
                new Quote { Id = "USDTJS", Source = "USD", Value = 13.041716 },
                new Quote { Id = "USDTMT", Source = "USD", Value = 3.51 },
                new Quote { Id = "USDTND", Source = "USD", Value = 2.947499 },
                new Quote { Id = "USDTOP", Source = "USD", Value = 2.279795 },
                new Quote { Id = "USDTRY", Source = "USD", Value = 14.794915 },
                new Quote { Id = "USDTTD", Source = "USD", Value = 6.784404 },
                new Quote { Id = "USDTWD", Source = "USD", Value = 28.590201 },
                new Quote { Id = "USDTZS", Source = "USD", Value = 2314.999835 },
                new Quote { Id = "USDUAH", Source = "USD", Value = 29.368564 },
                new Quote { Id = "USDUGX", Source = "USD", Value = 3599.158294 },
                new Quote { Id = "USDUSD", Source = "USD", Value = 1 },
                new Quote { Id = "USDUYU", Source = "USD", Value = 42.576811 },
                new Quote { Id = "USDUZS", Source = "USD", Value = 10981.70548 },
                new Quote { Id = "USDVEF", Source = "USD", Value = 213830222338.07285 },
                new Quote { Id = "USDVND", Source = "USD", Value = 22884 },
                new Quote { Id = "USDVUV", Source = "USD", Value = 114.116604 },
                new Quote { Id = "USDWST", Source = "USD", Value = 2.622046 },
                new Quote { Id = "USDXAF", Source = "USD", Value = 598.812339 },
                new Quote { Id = "USDXAG", Source = "USD", Value = 0.040469 },
                new Quote { Id = "USDXAU", Source = "USD", Value = 0.000519 },
                new Quote { Id = "USDXCD", Source = "USD", Value = 2.70255 },
                new Quote { Id = "USDXDR", Source = "USD", Value = 0.722176 },
                new Quote { Id = "USDXOF", Source = "USD", Value = 598.812339 },
                new Quote { Id = "USDXPF", Source = "USD", Value = 106.197564 },
                new Quote { Id = "USDYER", Source = "USD", Value = 250.250196 },
                new Quote { Id = "USDZAR", Source = "USD", Value = 15.11515 },
                new Quote { Id = "USDZMK", Source = "USD", Value = 9001.19908 },
                new Quote { Id = "USDZMW", Source = "USD", Value = 18.237678 },
                new Quote { Id = "USDZWL", Source = "USD", Value = 321.999592 }
            );
        }
    }
}
