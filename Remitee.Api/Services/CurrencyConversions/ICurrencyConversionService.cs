﻿namespace Remitee.Api.Services.CurrencyConversions
{
    public interface ICurrencyConversionService
    {
        double? CalculateOriginAmount(string originCurrency, string destinationCurrency, double destinationAmount);

        double? CalculateDestinationAmount(string originCurrency, double originAmount, string destinationCurrency);
    }
}
