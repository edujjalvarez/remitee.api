﻿using System;
using Remitee.Api.Models.Quotes;

namespace Remitee.Api.Services.CurrencyConversions
{
    public class CurrencyConversionService : ICurrencyConversionService
    {
        private readonly IQuoteRepository _quoteRepository;
        private const double Fee = 0.03; // 3%

        public CurrencyConversionService(IQuoteRepository quoteRepository)
        {
            _quoteRepository = quoteRepository;
        }


        public double? CalculateOriginAmount(string originCurrency, string destinationCurrency, double destinationAmount)
        {
            var quote = _quoteRepository.GetById($"{originCurrency}{destinationCurrency}");
            if (quote == null) return null;
            var originAmount = destinationAmount / (quote.Value - Fee);
            return originAmount;
        }

        public double? CalculateDestinationAmount(string originCurrency, double originAmount, string destinationCurrency)
        {
            var quote = _quoteRepository.GetById($"{originCurrency}{destinationCurrency}");
            if (quote == null) return null;
            var destinationAmount = originAmount * (quote.Value - Fee);
            return destinationAmount;
        }
    }
}
