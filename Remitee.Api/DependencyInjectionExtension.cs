﻿using Remitee.Api.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Remitee.Api.Models.Currencies;
using Remitee.Api.Models.Quotes;
using Remitee.Api.Services.CurrencyConversions;

namespace Remitee.Api
{
    public static class DependencyInjectionExtension
    {
        public static void InjectDependencies(this IServiceCollection services)
        {
            // Repositories
            services.AddScoped(typeof(IBaseRepository<>), typeof(BaseRepository<>));
            services.AddScoped(typeof(ICurrencyRepository), typeof(CurrencyRepository));
            services.AddScoped(typeof(IQuoteRepository), typeof(QuoteRepository));

            // Scoped Services
            services.AddScoped<ICurrencyConversionService, CurrencyConversionService>();

            // Transient Services
            services.AddTransient<IHttpContextAccessor, HttpContextAccessor>();
        }
    }
}
