﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace Remitee.Api
{
    public static class IdentityServerAuthentication
    {
        public static void AddIdentityServerAuthentication(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddAuthentication("Bearer")
                .AddJwtBearer("Bearer", options =>
                {
                    options.Authority = configuration["Authentication:IdentityServerHostSecureUrl"]; // Secure
                    //options.Authority = configuration["Authentication:IdentityServerHostInsecureUrl"]; // Insecure
                    options.Audience = "base.api";
                });
        }
    }
}
