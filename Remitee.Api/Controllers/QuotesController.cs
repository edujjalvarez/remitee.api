﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Remitee.Api.Models;
using Remitee.Api.Models.Quotes;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Remitee.Api.Services.CurrencyConversions;

namespace Remitee.Api.Controllers
{
    [Route("api/quotes")]
    public class QuotesController : BaseController<Quote>
    {
        private readonly IBaseRepository<Quote> _repository;
        private readonly ILogger<QuotesController> _logger;
        private readonly IQuoteRepository _quoteRepository;
        private readonly ICurrencyConversionService _currencyConversionService;

        public QuotesController(
            IBaseRepository<Quote> repository,
            ILogger<QuotesController> logger,
            IQuoteRepository quoteRepository,
            ICurrencyConversionService currencyConversionService) : base(repository, logger)
        {
            _repository = repository;
            _logger = logger;
            _quoteRepository = quoteRepository;
            _currencyConversionService = currencyConversionService;
        }

        [HttpGet, Route("source/{source}")]
        public ICollection<Quote> Get(string source)
        {
            return _repository.Get(e => e.Source.Equals(source));
        }

        [HttpGet, Route("async/source/{source}")]
        public async Task<ICollection<Quote>> GetAsync(string source)
        {
            return await _repository.GetAsync(e => e.Source.Contains(source));
        }

        [HttpGet, Route("calculate-origin-amount")]
        public IActionResult CalculateOriginAmount(string originCurrency, string destinationCurrency, double destinationAmount)
        {
            if (string.IsNullOrEmpty(originCurrency) || string.IsNullOrEmpty(destinationCurrency) || destinationAmount <= 0)
            {
                return BadRequest("Los parámetros originCurrency, destinationCurrency y destinationAmount son obligatorios.");
            }
            var originAmount = _currencyConversionService.CalculateOriginAmount(originCurrency, destinationCurrency,
                destinationAmount);
            if (originAmount == null) return BadRequest("No existe una cotización para las monedas ingresadas.");
            return Ok(originAmount);
        }

        [HttpGet, Route("calculate-destination-amount")]
        public IActionResult CalculateDestinationAmount(string originCurrency, double originAmount, string destinationCurrency)
        {
            if (string.IsNullOrEmpty(originCurrency) || string.IsNullOrEmpty(destinationCurrency) || originAmount <= 0)
            {
                return BadRequest("Los parámetros originCurrency, originAmount y destinationCurrency son obligatorios.");
            }
            var destinationAmount = _currencyConversionService.CalculateDestinationAmount(originCurrency, originAmount,
                destinationCurrency);
            if (destinationAmount == null) return BadRequest("No existe una cotización para las monedas ingresadas.");
            return Ok(destinationAmount);
        }
    }
}
